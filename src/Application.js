define([
    "lib/mini",
    "src/TitleScene",
    "src/GameScene",
    "src/NekoJumpGame",
    "src/ResultScene",
    "src/User"
],function( mini, TitleScene, GameScene, NekoJumpGame, ResultScene, User ){

    /**
     * @extend mini.Application
     * @constructor
     */
    var Application = function(){

        mini.Application.apply( this, arguments );

        this.config.debug = true;

        this.config.name         = "ねことび";
        this.config.urlscheme    = "nekojump";
        this.config.storeIOS     = "https://itunes.apple.com/us/app/nekotobi/id825346545?l=ja&ls=1&mt=8";
        this.config.storeAndroid = "https://play.google.com/store/apps/details?id=com.sonicmoov.mini.neko_jump";
        this.config.message      = "ベストスコア#best点！ #name #store";

        this.config.useHelp     = true;
        this.config.useSound    = false;
        this.config.titleScene  = TitleScene;
        this.config.gameScene   = GameScene;
        this.config.resultScene = ResultScene;
        this.config.game        = NekoJumpGame;
        this.config.user        = User;
        this.config.numberWidth = 57.5;

    };

    Application.prototype = Object.create( mini.Application.prototype, {

        createMessage:{ value: function( type ){
            return mini.Application.prototype.createMessage.apply( this, arguments );
        } }

    } );

    return Application;
});