define([
    "lib/mini"
], function (mini) {

    var GameScene = function () {
        mini.GameScene.apply(this, arguments);
    };

    GameScene.prototype = Object.create(mini.GameScene.prototype, {

        _onPrepare: { value: function () {

            //非表示コンテンツ
            this.backgroundImage.display.scaleX = this.backgroundImage.display.scaleY = 4;
        } },

        _onShow: { value: function () {
            //Any
        } },

        _onHide: { value: function () {
            //Any
        } }

    });

    return GameScene;
});