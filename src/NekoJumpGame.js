define([
    "lib/mini",
    "lib/assets"
], function (mini, Assets) {


    var NekoJumpGame = function () {
        mini.Game.apply(this, arguments);

        this._score = 0;
        this._scoreView = null;
    };

    NekoJumpGame.prototype = Object.create(mini.Game.prototype, {

        // property

        /**
         * ゲームスコア
         * @type Number
         */
        score: {
            get: function () {
                return this._score;
            },
            set: function (value) {

                // update scoreView
                this._score = value;

                var strValue = value + "";

                this._scoreView.value = strValue;

                // animation
                mini.Tween.removeTweens(this._scoreView.display);

                this._scoreView.display.scaleY = 1.4;
                this._scoreView.display.y = 47;
                mini.Tween.get(this._scoreView.display, { useTicks: true, group: "game" })
                    .to({ scaleX: 1, scaleY: 1, y: 50 }, 20, mini.Ease.elasticOut);
            }
        },
        /**
         * スコアView
         * @type mini.NumericImage
         */
        scoreView: { get: function () {
            return this._scoreView;
        } },

        // implements

        _onPrepare: { value: function () {

            // nav
            //  背景
            var black = this.foreground.addChildAt(new Bitmap(new BitmapData(1, 1, true, 0xff000000)), 0);
            black.width = 640;
            black.height = 100;
            black.alpha = 0.2;

            //  スコア
            this._scoreView = new mini.NumericImage(0);
            this.foreground.addChild(this._scoreView.display);
            this._scoreView.display.scaleX = this._scoreView.display.scaleY = 0.75;
            this._scoreView.display.transform.colorTransform = new ColorTransform(1, 1, 1);
            this._scoreView.x = 320;
            this._scoreView.y = 50;

            // 背景
            this._clouds = new RepeatableImage("./assets/clouds.png");
            this.background.addChild(this._clouds.display);
            this._clouds.x = 640 / 2;
            this._clouds.y = this.stage.stageHeight / 2 - 150;
            this._ground = new RepeatableImage("./assets/ground.png");
            this.background.addChild(this._ground.display);
            this._ground.x = 640 / 2;
            this._ground.y = this.stage.stageHeight - 100;


            var self = this;
            this._onTapBeginHandler = function () {
                self._onTapBegin();
            };
            this._onTapEndHandler = function () {
                self._onTapEnd();
            };

            //
            this._wallMaker = new WallMaker();
            this._objectList = [];
            this._objectContainer = new Sprite();

            // audio
            this._bgm = new Audio("./assets/bgm.mp3");
            this._bgm.loop = true;
            this._clear = new Audio("./assets/clear.mp3", "se");
            this._hit = new Audio("./assets/ok.wav", "se");

            // flash
            this._white = new Bitmap(new BitmapData(1, 1, true, 0xffffffff));
            this._white.width = this.stage.stageWidth;
            this._white.height = this.stage.stageHeight;

        } },

        _onInit: {
            value: function () {

                this.score = 0;

                // bg
                this._clouds.speedX = 0.2;
                this._ground.speedX = 0.5;

                // displays init
                this.container.addChild(this._objectContainer);
                while (this._objectList[0]) {
                    var object = this._objectList.shift();
                    this._objectContainer.removeChild(object.display);
                }

                if (this._character) this._character.reset();
                this._character = new Character(false, "12");
                this.container.addChild(this._character.display);


                this._ended = false;
                this._speed = 0.1;

                this._objectContainer.y = this.stage.stageHeight * ( -0.1 * (-0.5 + this._character.display.y / this.stage.stageHeight));

                this._wallCount = 0;
            }
        },

        _onStart: {
            value: function () {
                // listen events
                this.background.addEventListener("touchBegin", this._onTapBeginHandler);
                this.container.addEventListener("touchBegin", this._onTapBeginHandler);
                this.background.addEventListener("touchEnd", this._onTapEndHandler);
                this.container.addEventListener("touchEnd", this._onTapEndHandler);

                this._bgm.play();
            }
        },

        _onEnterFrame: { value: function (frameCount) {

            // ゲーム用tick
            mini.Tween.tick(1, "game");


            if (this._ended) return;


            // hit test
            for (var i = 0; i < this._objectList.length; i++) {
                var object = this._objectList[i];
                // hit test
                if (this._character.hitArea.hitTestObject(object.hitArea) != true) continue;
                object.fail();
                this._character.knockback();
                this._ended = true;

                this._bgm.stop();
                this._hit.play();

                // out
                var self = this;
                mini.Tween.get(this._character.display, { useTicks: true, group: "game" })
                    .wait(20)
                    .to({y: 1100}, 40, mini.Ease.quintIn)
                    .call(function () {
                        self.finish(self.score);
                    }).wait(0);
                mini.Tween.get(this._character.display, { useTicks: true, group: "game" })
                    .wait(0);

                return;
            }

            //

            this._character.level = this._speed * 0.04;

            this._clouds.speedX = this._speed * 4;
            this._ground.speedX = this._speed * 15;

            this._character.update();

            this._clouds.update();

            this._ground.update();


            // test object make
            if (frameCount > 80 && frameCount % 50 === 0) {


                var space = 220;//240;
                var val = space / this.stage.stageHeight;
                var mid = ( (1 - val) / 2 + 1.5 * (Math.random() - 0.5) * val ) * this.stage.stageHeight;

                var speed = 8;//9;

                var speedY = ( Math.random() - 0.5 ) > 0 ? 1 : -1;

                var flg = Math.random() > 0.5;

                // upper
                for (var i = 0; i < 4; i++) {
                    var object = this._wallMaker.make();
                    if (object) {
                        object.index = i;
                        this._objectContainer.addChild(object.display);
                        object.display.x = 800 + 20 * i;
                        object.display.rotation = 180;
                        object.display.scaleX *= -1;
                        object.speed = { x: -speed, y: speedY };
                        this._objectList.push(object);
                        var wallHeight = object.display.height - 50;
                        if (flg) {
                            object.display.y = mid - space * 2 - wallHeight * i;
                            mini.Tween.get(object.display, { useTicks: true, group: "game" }).wait(10).to({y: mid - space / 2 - wallHeight * i}, 60, mini.Ease.elasticOut);
                        } else {
                            object.display.y = mid - space - wallHeight * i;
                            mini.Tween.get(object.display, { useTicks: true, group: "game" }).wait(10).to({y: mid - space / 2 - wallHeight * i}, 30);//60,mini.Ease.elasticOut);
                        }

                        object.speed = { x: -speed, y: 0 };
                    }
                }

                for (var i = 0; i < 4; i++) {
                    var object = this._wallMaker.make();
                    if (object) {
                        object.index = i;
                        this._objectContainer.addChild(object.display);
                        object.display.x = 800 + 20 * i;
                        object.speed = { x: -speed, y: speedY };
                        this._objectList.push(object);
                        var wallHeight = object.display.height - 50;
                        if (flg) {
                            object.display.y = mid + space * 2 + wallHeight * i;
                            mini.Tween.get(object.display, { useTicks: true, group: "game" }).wait(10).to({y: mid + space / 2 + wallHeight * i}, 60, mini.Ease.elasticOut);
                        } else {
                            object.display.y = mid + space + wallHeight * i;
                            mini.Tween.get(object.display, { useTicks: true, group: "game" }).wait(10).to({y: mid + space / 2 + wallHeight * i}, 30);//60,mini.Ease.elasticOut);
                        }
                        object.speed = { x: -speed, y: 0 };
                    }
                }

                this._wallCount++;
            }

            // object update
            var removeList = [];
            var clear = false;
            for (var i = 0; i < this._objectList.length; i++) {
                var object = this._objectList[i];
                var prevX = object.display.x;
                object.update();
                var line = 80;
                if (object.index !== 1) continue;
                var newX = object.display.x;
                if (prevX >= line && newX < line) clear = true;
                if (newX < -100) removeList.push(object);
            }
            //
            if (clear) {
                this.score += 1;
                this._clear.play();
            }

            // object remove
            while (removeList[0]) {
                var object = removeList.shift();
                this._objectList.splice(this._objectList.indexOf(object), 1);
                this._objectContainer.removeChild(object.display);
            }

            // time up!!
            if (this._character.display.y > (this.stage.stageHeight - 180)) {

                this._ended = true;
                this._bgm.stop();
                this._hit.play();

                this._character.knockback();

                // out
                var self = this;
                mini.Tween.get(this._character.display, { useTicks: true, group: "game" })
                    .wait(30)
                    .call(function () {
                        self.finish(self.score);
                    })
                    .wait(0);
            }

        } },

        _onPause: { value: function () {
            this._bgm.pause();
        } },

        _onResume: { value: function () {
            this._bgm.resume();
        } },

        _onFinish: { value: function () {

            // unlisten events
            this.background.removeEventListener("touchBegin", this._onTapBeginHandler);
            this.container.removeEventListener("touchBegin", this._onTapBeginHandler);
            this.background.removeEventListener("touchEnd", this._onTapEndHandler);
            this.container.removeEventListener("touchEnd", this._onTapEndHandler);
        } },


        _onTapBegin: { value: function () {
            this._character.startFlap();
        } },

        _onTapEnd: { value: function () {
            this._character.endFlap();
        } }
    });


    //
    var Character = function (stop, charaId) {
        this._stop = arguments.length > 0 ? arguments[0] : false;
        this._id = charaId || "12";


        var self = this;
        var img = new Image("./assets/neko/" + this._id + ".png");
        img.onload = function () {
            self._bmp = new Bitmap(new BitmapData(this));
            self._init();
        };


        this._container = new Sprite();

        this._hitArea = new Bitmap(new BitmapData(1, 1, true, 0x00ff0000));
        this._hitArea.x = -34 + 40;
        this._hitArea.y = -20;
        this._hitArea.width = 65;
        this._hitArea.height = 50;
        this._container.addChild(this._hitArea);

        this._container.scaleX = this._container.scaleY = 1.2;

        this._frames = neko.frames;
        this._numFrames = 0;

        this._speed = { x: 0, y: 0 };

        this.isInitialized = false;

        // stop?
        if (this._stop) {
            this.speed = {x: 0, y: 0};
            var self = this;
            this.display.addEventListener("enterFrame", this._oef = function () {
                self.update();
            });
        }

        //
        this.display.y = 400;
        this._container.x = 100;
        this._speed.y = -10;
    };

    Character.prototype = Object.create({}, {

        id: { get: function () {
            return this._id;
        } },

        display: { get: function () {
            return this._container;
        } },

        reset: { value: function () {

            this.display.y = 400;
            this._container.x = 230;
            this._speed.y = -10;

            this.display.removeEventListener("enterFrame", this._oef);
            if (this.display.parent) this.display.parent.removeChild(this.display);
        } },

        _init: { value: function () {
            this.isInitialized = true;

            this._container.addChildAt(this._bmp, 0);
            this._bmp.x = -150 / 2 + 40;
            this._bmp.y = -100 / 2;


            this.update();
        } },

        update: { value: function () {
            if (!this.isInitialized) return;

            if (!this._stop) {
                // upper
                this._container.y += this._speed.y;
                if (this._container.y < 80) {
                    this._container.y = 80;
                    this._speed.y = 1;
                }
                //
                this._container.x += this._speed.x;

                // speed
                this._speed.y += 1.0;
                if (this._speed.y > 0)this._speed.y *= 1.01;

                this._container.rotation = Math.atan2(this._speed.y, 60) / Math.PI * 180;
            }


            var frame = this._frames[ "jump" ].frame;

            // fall?
            if (this._speed.y > 5) {
                this._numFrames++;
                frame = this._frames[ "fall_" + (1 + Math.floor(this._numFrames / 4) % 2) ].frame;
            }

//            console.log( JSON.stringify(frame) );

            this._bmp.setClippingRect(new Rectangle(frame.x, frame.y, frame.w, frame.h));

        } },

        startFlap: { value: function () {
            // filter

            this._speed.y = -16;
//            this._speed.y = -8;
        } },
        endFlap: { value: function () {

//            this._speed.y = 8;
        } },

        hitArea: { get: function () {
            return this._hitArea;
        } },

        knockback: { value: function () {
            //
            var frame = this._frames.fail.frame;
            log(JSON.stringify(frame));
            this._bmp.setClippingRect(new Rectangle(frame.x, frame.y, frame.w, frame.h));
        } }
    });
    NekoJumpGame.Character = Character;


    var WallMaker = function () {

        this.assets = new Assets({
            spriteSheet: new Image("./assets/wall_1.png")
        });

        this.isInialized = false;
        var self = this;
        this.assets.onload = function (assets) {
            self._spriteSheet = new BitmapData(assets.spriteSheet);
            self._init();
        };

    };
    WallMaker.prototype = Object.create({}, {
        _init: { value: function () {
            this.isInialized = true;
        } },

        make: { value: function () {
            if (!this.isInialized) return null;

            // TODO バリエーション
            return new Wall(this._spriteSheet, wall.frames);
        } }
    });

    var dummyBD = new BitmapData(1, 1, true, 0x00ff0000);

    var Wall = function (spriteSheet, frames) {

        this._bmp = new Bitmap(spriteSheet);
        this._frames = frames;

        this._container = new Sprite();
        this._container.scaleX = this._container.scaleY = 1.2;

        this._sprite = this._bmp;
        this._sprite.x = -188 / 2;
        this._sprite.y = 0;
        this._container.addChild(this._sprite);

        this._hitArea = new Bitmap(dummyBD);
        this._hitArea.width = 80;
        this._hitArea.height = 160;
        this._hitArea.x = -(80) / 2 - 30;
        this._hitArea.y = 30;
        this._container.addChild(this._hitArea);


        this._init();
    };

    Wall.prototype = Object.create({}, {

        display: { get: function () {
            return this._container;
        } },

        _init: { value: function () {
            var self = this;
            //
            var frame = this._frames.stand.frame;
            this._bmp.setClippingRect(new Rectangle(frame.x, frame.y, frame.w, frame.h));
        } },

        update: { value: function () {
            this._container.x += this._speed.x;
            this._container.y += this._speed.y;
        } },

        fail: { value: function () {
            var frame = this._frames.fail.frame;
            this._bmp.setClippingRect(new Rectangle(frame.x, frame.y, frame.w, frame.h));
        } },

        speed: { get: function () {
            return this._speed;
        }, set: function (value) {
            this._speed = value
        } },

        hitArea: { get: function () {
            return this._hitArea;
        } }
    });


    var RepeatableImage = function () {
        mini.ImageView.apply(this, arguments);
        this._bmp2 = null;
        this._speedX = 0;
    };
    RepeatableImage.prototype = Object.create(mini.ImageView.prototype, {
        width: {
            get: function () {
                return this._bmp.width;
            },
            set: function (value) {
                this._bmp.width = this._bmp2.width = value;
                this._bmp.x = -this._bmp.width / 2;
            }
        },
        height: {
            get: function () {
                return this._bmp.height;
            },
            set: function (value) {
                this._bmp.height = this._bmp2.height = value;
                this._bmp.y = -this._bmp.height / 2;
            }
        },

        // callback
        onLoad: { value: null },

        // internal method
        _onImageLoaded: { value: function (image) {
            this._bmp = new Bitmap(new BitmapData(image));
            this._bmp.x = -this._bmp.width / 2;
            this._bmp.y = -this._bmp.height / 2;
            this._display.addChild(this._bmp);

            this._bmp2 = new Bitmap(this._bmp.bitmapData);
            this._bmp2.x = -this._bmp2.width / 2 + this._bmp.width;
            this._bmp2.y = -this._bmp2.height / 2;
            this._display.addChild(this._bmp2);

            if (this.onLoad !== null) this.onLoad();
        } },

        // method
        dispose: { value: function () {
            // TODO
        } },

        speedX: { get: function () {
            return this._speedX;
        }, set: function (val) {
            this._speedX = val;
        } },

        update: { value: function () {
            if (!this._bmp)return;
            //
            this._bmp.x -= this._speedX;
            this._bmp2.x -= this._speedX;

            if (this._bmp.x < -this._bmp.width * 1.5) this._bmp.x = this._bmp2.x + this._bmp2.width;
            if (this._bmp2.x < -this._bmp2.width * 1.5) this._bmp2.x = this._bmp.x + this._bmp.width;

        } }
    });


    return NekoJumpGame;
});


neko = {"frames": {

    "stand": {
        "frame": {"x": 0, "y": 0, "w": 128, "h": 103},
        "rotated": false,
        "trimmed": false,
        "spriteSourceSize": {"x": 0, "y": 0, "w": 128, "h": 103},
        "sourceSize": {"w": 128, "h": 103}
    },
    "jump": {
        "frame": {"x": 128, "y": 0, "w": 128, "h": 103},
        "rotated": false,
        "trimmed": false,
        "spriteSourceSize": {"x": 0, "y": 0, "w": 128, "h": 103},
        "sourceSize": {"w": 128, "h": 103}
    },
    "to_jump": {
        "frame": {"x": 0, "y": 103, "w": 128, "h": 103},
        "rotated": false,
        "trimmed": false,
        "spriteSourceSize": {"x": 0, "y": 0, "w": 128, "h": 103},
        "sourceSize": {"w": 128, "h": 103}
    },
    "fall_1": {
        "frame": {"x": 128, "y": 103, "w": 128, "h": 103},
        "rotated": false,
        "trimmed": false,
        "spriteSourceSize": {"x": 0, "y": 0, "w": 128, "h": 103},
        "sourceSize": {"w": 128, "h": 103}
    },
    "fall_2": {
        "frame": {"x": 0, "y": 206, "w": 128, "h": 103},
        "rotated": false,
        "trimmed": false,
        "spriteSourceSize": {"x": 0, "y": 0, "w": 128, "h": 103},
        "sourceSize": {"w": 128, "h": 103}
    },
    "fail": {
        "frame": {"x": 128, "y": 206, "w": 128, "h": 103},
        "rotated": false,
        "trimmed": false,
        "spriteSourceSize": {"x": 0, "y": 0, "w": 128, "h": 103},
        "sourceSize": {"w": 128, "h": 103}
    }},
    "meta": {
        "app": "Adobe Flash CS6",
        "version": "12.0.0.481",
        "image": "neko.png",
        "format": "RGBA8888",
        "size": {"w": 256, "h": 326},
        "scale": "1"
    }
};


wall = {"frames": {

    "stand": {
        "frame": {"x": 0, "y": 0, "w": 157, "h": 200},
        "rotated": false,
        "trimmed": false,
        "spriteSourceSize": {"x": 0, "y": 0, "w": 157, "h": 200},
        "sourceSize": {"w": 157, "h": 200}
    },
    "sleep": {
        "frame": {"x": 157, "y": 0, "w": 157, "h": 200},
        "rotated": false,
        "trimmed": false,
        "spriteSourceSize": {"x": 0, "y": 0, "w": 157, "h": 200},
        "sourceSize": {"w": 157, "h": 200}
    },
    "fail": {
        "frame": {"x": 314, "y": 0, "w": 157, "h": 200},
        "rotated": false,
        "trimmed": false,
        "spriteSourceSize": {"x": 0, "y": 0, "w": 157, "h": 200},
        "sourceSize": {"w": 157, "h": 200}
    }},
    "meta": {
        "app": "Adobe Flash CS6",
        "version": "12.0.0.481",
        "image": "wall_1.png",
        "format": "RGBA8888",
        "size": {"w": 488, "h": 211},
        "scale": "1"
    }
};

