define([
    "lib/mini"
], function (mini) {

    var ResultScene = function () {
        mini.ResultScene.apply(this, arguments);
    };

    ResultScene.prototype = Object.create(mini.ResultScene.prototype, {

        _onPrepare: {
            value: function () {

                log("_onPrepare");
                // レイアウト
                this.popupBG.display.y -= 43;

                this.popupScoreView.display.scaleX = this.popupScoreView.display.scaleY = 1.5;

                this._popup.removeChild(this._popupBestScoreView.display);
                this._popupBestScoreView = new mini.NumericImage(1234, "./assets/result_number_best.png", 37.5);
                this._popupBestScoreView.display.scaleX = this._popupBestScoreView.display.scaleY = 0.9;
                this._popup.addChild(this._popupBestScoreView.display);

                // y
                var offsetY = -240;
                this.popupScoreView.y = offsetY + 37;
                this.popupBestScoreView.y = offsetY - 2;

                // x
                var offsetX = -140;
                this.popupScoreView.x = offsetX + 20;
                this.popupBestScoreView.x = offsetX + 310;

                // snsボタンを移動
                var fbSprite = this.popup.addChild(this.btnFacebook.display);
                var twSprite = this.popup.addChild(this.btnTwitter.display);
                var lnSprite = this.popup.addChild(this.btnLine.display);

                fbSprite.x -= this.stage.stageWidth / 2 + 123;
                twSprite.x -= this.stage.stageWidth / 2 + 123;
                lnSprite.x -= this.stage.stageWidth / 2 + 123;

                fbSprite.y = twSprite.y = lnSprite.y = -3;

                // se
                ResultScene.se = new Audio("./assets/ok.wav", "se");
            }
        },

        _onUpdateNewRecord: {
            value: function (score) {
                //any
            }
        },

        _onShow: {
            value: function () {
                this.stage.addEventListener("enterFrame", this._oef = function () {
                    mini.Tween.tick(1, "result");
                });
            }
        },
        _onHide: {
            value: function () {
                this.stage.removeEventListener("enterFrame", this._oef);
            }
        }

    });

    return ResultScene;
});