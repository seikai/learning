define([
    "lib/mini"
], function (mini) {

    var User = function () {
        mini.User.apply(this, arguments);
    };

    User.prototype = Object.create(mini.User.prototype, {

        compareScore: {
            value: function (a, b) {
                return a >= b;
            }
        },

        /**
         *
         */
        playCount: {
            get: function () {
                return this._data.hasOwnProperty("playCount") ? this._data.playCount : 0;
            },
            set: function (value) {
                this._data.playCount = value;
                this._save();
            }
        },

        /**
         * キャラ選択
         */
        activeCharaId: {
            get: function () {
                return this._data.activeCharaId || "normal";
            },
            set: function (name) {
                this._data.activeCharaId = name;
                this._save();
            }
        }

    });

    return User;
});