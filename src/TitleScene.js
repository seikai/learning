define([
    "lib/mini"
],function( mini ){

    var TitleScene = function(){
        mini.TitleScene.apply( this, arguments );
    };

    TitleScene.prototype = Object.create( mini.TitleScene.prototype ,{

        _onPrepare: { value: function(){
            //this.buttonStart.x += 120;
        } },

        _onShow: { value: function(){
            //any
        } },

        _onHide: { value: function(){
			//any
        } }

    });

    return TitleScene;
});