define([
    "lib/scene",
    "lib/make"
], function (Scene, make) {

    /**
     * ミニゲーム向けフレームワーク
     */
    var mini = {};

    /**
     * 設定・構成オブジェクト
     * @constructor
     */
    var Config = function () {

        // 設定
        this.debug = true;
        this.initPath = "/";
        this.width = 640;
        this.height = (  window.innerHeight * 640 / window.innerWidth );
        this.orientation = "portrait";
        this.name = "開発中ゲーム";
        this.message = "スコア#score ベスト#best #name #store";

        // 機能オプション
        this.useSound = true;

        // シーンクラス
        this.titleScene  = TitleScene;
        this.gameScene   = GameScene;
        this.resultScene = ResultScene;

        // モデルクラス
        this.user = User;

        // ゲームクラス デフォルトはnull GameSceneにゲームを実装するか、Gameクラスを使うかは実装者次第
        this.game = null;

        // タイトルシーン

        // ゲームシーン
        this.gameOverlayColor = 0xff000000;

        // 結果シーン


        // 画像情報

        //  数字
        this._numbers = "./assets/mini/_numbers.png"; // "0123456789." or "単位0123456789." の画像。小数点以外等幅の前提
        this.numberWidth = 51; // 数字の幅px。画像の横幅をこれで割った余りを小数点画像とする

        //  SNSボタン
        this._btn_facebook = "./assets/mini/_btn_facebook.png";
        this._btn_line = "./assets/mini/_btn_line.png";
        this._btn_twitter = "./assets/mini/_btn_twitter.png";

        //  ゲームシーン画像
        this.game_bg = "./assets/mini/game_bg.png";
        this.game_btn_pause = "./assets/mini/game_btn_pause.png";
        this.game_popup_bg = "./assets/mini/game_popup_bg.png";
        this.game_popup_btn_return_to_game = "./assets/mini/game_popup_btn_return_to_game.png";
        this.game_popup_btn_to_title = "./assets/mini/game_popup_btn_to_title.png";

        //  結果シーン画像
        this.result_bg = "./assets/mini/result_bg.png";
        this.result_btn_retry = "./assets/mini/result_btn_retry.png";
        this.result_btn_to_title = "./assets/mini/result_btn_to_title.png";
        this.result_popup_bg = "./assets/mini/result_popup_bg.png";

        //  タイトルシーン画像
        this.title_bg = "./assets/mini/title_bg.png";
        this.title_btn_start = "./assets/mini/title_btn_start.png";
    };

    Config.prototype = Object.create({}, {
        //any
    });

    mini.Config = Config;


    /********************************************************************************************************************
     * Controller
     */

    /**
     * アプリケーション
     * @param config
     * @constructor
     */
    var Application = function (config) {
        window.application = this;
        this._config = config || new Config();

        var self = this;
        window.log = function () {
            self.log.apply(self, arguments);
        };
    };
    Application.prototype = Object.create({}, {

        /**
         * アプリの設定
         * @type mini.Config
         */
        config: {
            get: function () {
                return this._config;
            },
            set: function (value) {
                this._config = value;
            }
        },

        /**
         * ユーザー
         * @type mini.User
         */
        user: { value: null },

        /**
         * ステージ
         * @type Stage
         */
        stage: { value: null },

        /**
         * ログ
         * @aliase window.log()
         */
        log: { value: function () {
            console.log.apply(console, arguments);
        } },

        /**
         * アプリ起動
         */
        start: { value: function () {
            log("start Application");

            var config = this.config;

            // stage TODO
            this.layer = addLayer(new Stage(config.width, config.height));
            this.layer.scaleMode = Layer.SCALE_NO_BORDER;
            this.stage = this.layer.content;

            // tween
            this.stage.addEventListener("enterFrame", function () {
                mini.Tween.tick();
            });

            // 画面方向
            window.setOrientationType(config.orientation || "portrait");

            // ボタン音
            if (config.useSound) {
                // TODO mini自体の音対応
                Button.buttonAudio = new Audio("./assets/mini/button.mp3", "se");
            }

            // ユーザー
            this.user = new config.user();
            this.user._launch();// 起動

            if (this.user.launchCount === 1) {
                // 初回起動
                this._onFirstLaunch();
            }

            // アプリバージョンチェック
            if (app.buildVersion) { // buildVersionのAPIがあれば
                var latestAppBuildVersion = localStorage.getItem("app_buildVersion") || null;
                var buildVersion = app.buildVersion;
                if (buildVersion != latestAppBuildVersion) {
                    // アプリ更新があったと推測できるので
                    this._onChangeAppVersion(latestAppBuildVersion, buildVersion);
                }
                localStorage.setItem("app_buildVersion", buildVersion);
            }


            // DEBUG mode
            if (this.config.debug !== true) {
                // RELEASE
                //  デバッグ機能の削除
                this.log = function () {
                };
                if (window.devtools) devtools.view.visible = false;
            }
            else {
                // DEBUG
                //  デバッグ機能を有効化
                new DebugFooter();
                //  表示中画面で再読み込み機能
                var params = {};
                location.search.substr(1).split("&").forEach(function (value) {
                    var tmp = value.split("=");
                    params[ tmp[0] ] = tmp[1];
                });
                if (params.dev_init_path) this.config.initPath = params.dev_init_path;
            }

            // シーンの設定
            Scene.mapping({
                "/": new config.titleScene(),
                "/game": new config.gameScene(),
                "/result": new config.resultScene()
            });

            Scene.goto(this.config.initPath);

        }},

        /**
         * SNS向けメッセージの生成
         */
        createMessage: { value: function (type) {
            var msg = this.config.message;
            msg = msg.replace(/#name/g, this.config.name);
            msg = msg.replace(/#score/g, this.user.score + "");
            msg = msg.replace(/#best/g, this.user.bestScore + "");

            if (type !== "facebook")
                msg = msg.replace(/#store/g, this.config[ "store" + ( app.isIOS ? "IOS" : "Android" ) ]);
            else
                msg = msg.replace(/#store/g, "");

            if (type === "twitter") msg += " #" + this.config.name;

            return msg;
        } },


        // Template methods
        /**
         * 初回起動時に呼ばれる
         */
        _onFirstLaunch: { value: function () {
        } },
        /**
         * アプリ本体アップデート後最初の起動で呼ばれる
         * @param {String} from アップデート前のバージョン。もしくはnull(初回かapp.buildVersion非対応だったか)
         * @param {String} to 現在のバージョン
         */
        _onChangeAppVersion: { value: function (from, to) {
        } }
    });
    mini.Application = Application;


    var SCENE_CHANGE_FRAMES = 20;
    /**
     * miniのシーン基底
     * @constructor
     */
    var MiniScene = function () {
        Scene.apply(this, arguments);

        this._rootContainer = new Sprite();
        this._cover = new Bitmap(new BitmapData(1, 1, true, 0xff000000));
        this._cover.name = "cover";

        this._background = this._rootContainer.addChild(new Sprite());
        this._background.name = "background";
        this._container = this._rootContainer.addChild(new Sprite());
        this._container.name = "container";
        this._foreground = this._rootContainer.addChild(new Sprite());
        this._foreground.name = "foreground";
    };
    MiniScene.prototype = Object.create(Scene.prototype, {

        // callback

        onPrepare: { value: function () {
            log("MiniScene.onPrepare");
            this._rootContainer.name = ("scene_root:" + this.path);
            this._onPrepare();
            this.completePrepare();
        } },

        /**
         * prepare処理
         * @protected
         */
        _onPrepare: { value: function () {
        } },

        onShow: { value: function () {

            this._onShow();
            this._cover.width = this.stage.stageWidth;
            this._cover.height = this.stage.stageHeight;

            this._rootContainer.mouseChildren = false;

            var self = this;
            mini.Tween.removeTweens(this._cover);

            mini.Tween
                .get(this._cover, { useTicks: true })
                .wait(SCENE_CHANGE_FRAMES / 2)
                .call(function () {
                    self.stage.addChild(self._rootContainer);
                    self.stage.addChild(self._cover);
                    self._cover.alpha = 1;
                })
                .to({ alpha: 0 }, SCENE_CHANGE_FRAMES / 2, mini.Ease.sineOut)
                .call(function () {
                    self.stage.removeChild(self._cover);
                    self._rootContainer.mouseChildren = true;
                })
                .wait(0);
        } },

        /**
         * show処理
         * @protected
         */
        _onShow: { value: function () {
        } },

        onHide: { value: function () {
            this._onHide();
            this.stage.addChild(this._cover);

            this._rootContainer.mouseChildren = false;

            mini.Tween.removeTweens(this._cover);

            var self = this;
            this._cover.alpha = 0;
            mini.Tween
                .get(this._cover, { useTicks: true })
                .to({ alpha: 1 }, SCENE_CHANGE_FRAMES / 2, mini.Ease.sineOut)
                .call(function () {
                    self.stage.removeChild(self._rootContainer);
                    self.stage.removeChild(self._cover);
                }).wait(0);
        } },

        /**
         * hide処理
         * @protected
         */
        _onHide: { value: function () {
        } },

        // property

        /**
         * ユーザー情報
         * @type mini.User
         */
        user: { get: function () {
            return application.user;
        } },
        /**
         * 構成・設定オブジェクト
         * @type Object
         */
        config: { get: function () {
            return application.config;
        } },
        /**
         * 表示ステージ
         * @type Stage
         */
        stage: { get: function () {
            return application.stage;
        } },
        /**
         * シーン別 表示ルートコンテナ
         * @type Sprite
         */
        rootContainer: { get: function () {
            return this._rootContainer;
        } },
        /**
         * 背景表示コンテナ
         * @type Sprite
         */
        background: { get: function () {
            return this._background;
        } },
        /**
         * 表示コンテナ
         * @type Sprite
         */
        container: { get: function () {
            return this._container;
        } },
        /**
         * 前景表示コンテナ
         * @type Sprite
         */
        foreground: { get: function () {
            return this._foreground;
        } },


        // method

        /**
         * シーン遷移
         * @type string シーンパス
         */
        goto: { value: function (path) {
            Scene.goto(path);
        } }
    });
    mini.MiniScene = MiniScene;


    /********************************************************************************************************************
     * Controller Sceneテンプレート実装
     * ・アプリケーションスキーム定義
     * ・実装上のマネタイズノウハウ共有の場
     */

    /**
     * タイトルシーン
     * @constructor
     */
    var TitleScene = function () {
        MiniScene.apply(this, arguments);
        log("new TitleScene()");
    };

    TitleScene.prototype = Object.create(MiniScene.prototype, {

        onPrepare: { value: function () {

            this._bgImage  = new ImageView(this.config.title_bg);
            this._btnStart = new Button(this.config.title_btn_start, "タイトル画面:スタートボタン");

            this._background.addChild(this._bgImage.display);
            this._bgImage.display.x = this.stage.stageWidth / 2;
            this._bgImage.display.y = this.stage.stageHeight / 2;

            var span = 110;
            var offset = 92;
            this._foreground.addChild(this._btnStart.display);
            this._btnStart.display.x = this.stage.stageWidth / 2;
            this._btnStart.display.y = this.stage.stageHeight / 2 + offset + span * 0;
            this._btnStart.onTap = function () {
                Scene.goto("/game");
            };


            MiniScene.prototype.onPrepare.call(this);

        } },

        onShow: { value: function () {
            MiniScene.prototype.onShow.call(this);
        } },
        onHide: { value: function () {
            MiniScene.prototype.onHide.call(this);
        } },


        // parts
        /**
         * 背景
         * @type mini.ImageView
         */
        backgroundImage: { get: function () {
            return this._bgImage;
        } },
        /**
         * スタートボタン
         * @type mini.Button
         */
        buttonStart: { get: function () {
            return this._btnStart;
        } }


    });
    mini.TitleScene = TitleScene;


    /**
     * ゲームシーン
     * @constructor
     */
    var GameScene = function () {
        MiniScene.apply(this, arguments);
        log("new GameScene()");
        // property values
        this._frameCount = 0;

        this._hasGame = false;

        var self = this;
        // callback internal
        this._enterFrameHander = function () {
            self._onEnterFrame(self._frameCount);
            self._frameCount++;
        };
        this._pauseHander = function () {
            log("pauseHandler");


            self.pauseGame();
        }
    };

    GameScene.prototype = Object.create(MiniScene.prototype, {

        onPrepare: { value: function () {

            this._bgImage = new ImageView(this.config.game_bg);
            this._btnPause = new Button(this.config.game_btn_pause, "ゲーム画面:一時停止ボタン");

            this._overlay = new Sprite();
            this._overlay.addChild(new Bitmap(new BitmapData(1, 1, true, this.config.gameOverlayColor))); // TODO 画像入れたい
            this._overlay.alpha = 0.5;
            this._overlay.width = this.stage.stageWidth;
            this._overlay.height = this.stage.stageHeight;

            // popup
            this._popup = new Sprite();
            this._popupBG = new mini.ImageView(this.config.game_popup_bg);
            this._popupBtnToTitle = new mini.Button(this.config.game_popup_btn_to_title, "ゲーム画面:タイトルへボタン");
            this._popupBtnReturnToGame = new mini.Button(this.config.game_popup_btn_return_to_game, "ゲーム画面:ゲームに戻るボタン");


            var self = this;

            this._background.addChild(this._bgImage.display);
            this._bgImage.display.x = this.stage.stageWidth / 2;
            this._bgImage.display.y = this.stage.stageHeight / 2;

            this._foreground.addChild(this._btnPause.display);
            this._btnPause.display.x = 11 + 72 / 2;
            this._btnPause.display.y = 11 + 78 / 2;//this.stage.stageHeight/2;
            this._btnPause.onTap = function () {
                self.pauseGame()
            };

            // overlay pause
            this._popup.x = this.stage.stageWidth / 2;
            this._popup.y = this.stage.stageHeight - 320;

            this._popup.addChild(this._popupBG.display);

            var offset = -160;

            this._popup.addChild(this._popupBtnReturnToGame.display);
            this._popupBtnReturnToGame.display.y = 90 + offset;
            this._popupBtnReturnToGame.onTap = function () {
                self.resumeGame();
            };

            this._popup.addChild(this._popupBtnToTitle.display);
            this._popupBtnToTitle.display.y = 185 + offset;
            this._popupBtnToTitle.onTap = function () {
                Scene.goto("/");
            };


            // Game
            if (this.config.game) {
                this._game = new this.config.game(this);
                this._hasGame = true;

                this._game._onPrepare();
            }

            MiniScene.prototype.onPrepare.call(this);

        } },

        onShow: { value: function () {
            // ゲーム初期化
            this._onInitGame();
            MiniScene.prototype.onShow.call(this);

            // pause
            this._foreground.removeChild(this._overlay);
            this._foreground.removeChild(this._popup);


            this.startGame();
        } },

        onHide: { value: function () {
            MiniScene.prototype.onHide.call(this);
        } },


        // GAME LifeCycle

        /**
         * ゲームの初期化処理を実装する
         */
        _onInitGame: { value: function () {
            log("game init");
            if (this._hasGame) this._game._onInit();
        } },
        /**
         * ゲームの開始処理を実装する
         */
        _onStartGame: { value: function () {
            log("game start");
            if (this._hasGame) this._game._onStart();
        } },
        /**
         * ゲームの一時停止処理を実装する
         */
        _onPauseGame: { value: function () {
            log("game pause");
            if (this._hasGame) this._game._onPause();
        } },
        /**
         * ゲームの再開処理を実装する
         */
        _onResumeGame: { value: function () {
            log("game resume");
            if (this._hasGame) this._game._onResume();
        } },
        /**
         * ゲームの終了処理を実装する
         */
        _onFinishGame: { value: function () {
            log("game finish");
            if (this._hasGame) this._game._onFinish();
        } },
        /**
         * 毎フレームの処理を実装する
         */
        _onEnterFrame: { value: function (frameCount) {
            if (this._hasGame) this._game._onEnterFrame(frameCount);
        } },

        // property
        /**
         * startからのフレーム数
         */
        frameCount: { get: function () {
            return this._frameCount || 0;
        } },


        // method

        /**
         * ゲームを開始する
         */
        startGame: { value: function () {

            this._onStartGame();

            this._frameCount = 0;

            // enterFrame
            this.stage.addEventListener("enterFrame", this._enterFrameHander);
            app.addEventListener("pause", this._pauseHander);
        } },
        /**
         * ゲームを一時停止させる
         */
        pauseGame: { value: function () {

            this._foreground.addChild(this._overlay);
            this._foreground.addChild(this._popup);

            mini.Tween.removeTweens(this._overlay);
            mini.Tween.removeTweens(this._popup);
            this._overlay.alpha = 0;
            mini.Tween
                .get(this._overlay, { useTicks: true })
                .to({ alpha: 0.5 }, 8, mini.Ease.sineOut);
            this._popup.alpha = 0;
            this._popup.y = this.stage.stageHeight - 320 + 40;
            mini.Tween
                .get(this._popup, { useTicks: true })
                .to({ alpha: 1, y: this.stage.stageHeight - 320 }, 8, mini.Ease.circOut);

            //
            this._onPauseGame();

            // enterFrame
            this.stage.removeEventListener("enterFrame", this._enterFrameHander);
        } },
        /**
         * ゲームを再開させる
         */
        resumeGame: { value: function () {

            var self = this;

            mini.Tween.removeTweens(this._overlay);
            mini.Tween.removeTweens(this._popup);
            mini.Tween
                .get(this._overlay, { useTicks: true })
                .to({ alpha: 0.0 }, 8, mini.Ease.sineIn)
                .call(function () {
                    self._foreground.removeChild(self._overlay);
                })
                .wait(0);
            mini.Tween
                .get(this._popup, { useTicks: true })
                .to({ alpha: 0.0, y: this.stage.stageHeight - 320 + 40 }, 8, mini.Ease.circIn)
                .call(function () {
                    self._foreground.removeChild(self._popup);

                    // resume game
                    self._onResumeGame();
                    // enterFrame
                    self.stage.addEventListener("enterFrame", self._enterFrameHander);
                })
                .wait(0);

        } },
        /**
         * ゲームを終了する
         * @param {Number} score スコア指定なければ結果画面へ遷移しない
         */
        finishGame: { value: function (score) {
            //
            this._onFinishGame();

            this._rootContainer.mouseChildren = false;

            // stop enterFrame
            this.stage.removeEventListener("enterFrame", this._enterFrameHander);
            app.removeEventListener("pause", this._pauseHander);


            // playCount
            this.user.playCount++;

            // goto result
            if (arguments.length > 0) // 互換
                this.goto("/result?score=" + score);
        } },


        // parts
        /**
         * 背景
         * @type mini.ImageView
         */
        backgroundImage: { get: function () {
            return this._bgImage;
        } },
        /**
         * 一時停止ボタン
         * @type mini.Button
         */
        buttonPause: { get: function () {
            return this._btnPause;
        } },
        /**
         * ポップアップ
         * @type Sprite
         */
        popup: { get: function () {
            return this._popup;
        } },
        /**
         * ポップアップ背景
         * @type mini.ImageView
         */
        popupBG: { get: function () {
            return this._popupBG;
        } },
        /**
         * ポップアップ内 タイトルへボタン
         * @type mini.Button
         */
        popupBtnToTitle: { get: function () {
            return this._popupBtnToTitle;
        } },
        /**
         * ポップアップ内 ゲームに戻るボタン
         * @type mini.Button
         */
        popupBtnReturnToGame: { get: function () {
            return this._popupBtnReturnToGame;
        } }
    });
    mini.GameScene = GameScene;


    /**
     * 結果シーン
     * @constructor
     */
    var ResultScene = function () {
        MiniScene.apply(this, arguments);
        log("new ResultScene()");
    };
    ResultScene.prototype = Object.create(MiniScene.prototype, {

        onPrepare: { value: function () {
            log("ResultScene.onPrepare");

            this._bgImage = new ImageView(this.config.result_bg);
            this._popup = new Sprite();
            this._popupBG = new ImageView(this.config.result_popup_bg);
            this._btnToTitle = new Button(this.config.result_btn_to_title, "結果画面:タイトルへボタン");
            this._btnRetry = new Button(this.config.result_btn_retry, "結果画面:リトライボタン");
            this._btnLine = new Button(this.config._btn_line, "結果画面:Lineボタン");
            this._btnFacebook = new Button(this.config._btn_facebook, "結果画面:Facebookボタン");
            this._btnTwitter = new Button(this.config._btn_twitter, "結果画面:Twitterボタン");

            this._popupScoreView = new NumericImage();
            this._popupBestScoreView = new NumericImage();

            var self = this;

            this._background.addChild(this._bgImage.display);
            this._bgImage.display.x = this.stage.stageWidth / 2;
            this._bgImage.display.y = this.stage.stageHeight / 2;

            this._popup.x = this.stage.stageWidth / 2;
            this._popup.y = this.stage.stageHeight / 2;
            this._foreground.addChild(this._popup);

            this._popup.addChild(this._popupBG.display);
            this._popupBG.display.y = -115;

            this._popup.addChild(this._popupScoreView.display);
            this._popupScoreView.display.y = -152;
            this._popupScoreView.value = 0;

            this._popup.addChild(this._popupBestScoreView.display);
            this._popupBestScoreView.display.y = -13;
            this._popupBestScoreView.display.scaleX = this._popupBestScoreView.display.scaleY = 0.7;
            this._popupBestScoreView.value = 0;


            var margin = 12;
            var offset = 100 / 2 + margin;

            offset += 72 / 2 + margin;
            this._foreground.addChild(this._btnLine.display);
            this._btnLine.display.x = 640 - offset;
            this._btnLine.display.y = this.stage.stageHeight / 2 - 580 + 200 + margin + 78 / 2;
            this._btnLine.onTap = function () {
                line.postMessage(application.createMessage("line"));
            };

            offset += 72 / 2 + 72 / 2 + margin;
            this._foreground.addChild(this._btnTwitter.display);
            this._btnTwitter.display.x = 640 - offset;
            this._btnTwitter.display.y = this.stage.stageHeight / 2 - 580 + 200 + margin + 78 / 2;
            this._btnTwitter.onTap = function () {
                twitter.postMessage(application.createMessage("twitter"));
            };

            offset += 72 / 2 + 72 / 2 + margin;
            this._foreground.addChild(this._btnFacebook.display);
            this._btnFacebook.display.x = 640 - offset;
            this._btnFacebook.display.y = this.stage.stageHeight / 2 - 580 + 200 + margin + 78 / 2;
            this._btnFacebook.onTap = function () {
                var message = application.createMessage("facebook");
                var storeURL = self.config[ "store" + ( app.isIOS ? "IOS" : "Android" ) ];
                facebook.postFeed(self.config.name, storeURL, message);
            };

            // TODO androidでfacebook投稿がイマイチなので
            if (app.isANDROID) this._btnFacebook.display.visible = false;


            this._foreground.addChild(this._btnToTitle.display);
            this._btnToTitle.display.x = -140 + this.stage.stageWidth / 2;
            this._btnToTitle.display.y = 310 + this.stage.stageHeight / 2;
            this._btnToTitle.onTap = function () {
                self.goto("/");
            };

            this._foreground.addChild(this._btnRetry.display);
            this._btnRetry.display.x = 140 + this.stage.stageWidth / 2;
            this._btnRetry.display.y = 310 + this.stage.stageHeight / 2;
            this._btnRetry.onTap = function () {
                self.goto("/game");
            };


            MiniScene.prototype.onPrepare.call(this);
        } },

        onShow: { value: function () {

            // score
            this.user.score = this.parameters.score;
            this._popupScoreView.value = this.user.score;
            this._popupBestScoreView.value = this.user.bestScore;

            // popup animation
            var self = this;
            this._popup.alpha = 0.01;
            this._popup.scaleX = this._popup.scaleY = 3;
            mini.Tween
                .get(this._popup, { useTicks: true })
                .wait(20)
                .to({ scaleX: 0.9, scaleY: 0.9, alpha: 1 }, 9)
                .to({ scaleX: 1, scaleY: 1 }, 6, mini.Ease.sineOut)
                .wait(10)
                .call(function () {
                    // finish fade animation

                    // new record?
                    if (self.user.score === self.user.bestScore) {
                        // new record!
                        self._onUpdateNewRecord(self.user.score);
                    }

                }).wait(0);

            MiniScene.prototype.onShow.call(this);

        } },
        onHide: { value: function () {
            MiniScene.prototype.onHide.call(this);
        } },

        /**
         * ベスト更新時
         */
        _onUpdateNewRecord: { value: function (score) {
            log("new record: " + score);
        }},

        // parts
        /**
         * 背景
         * @type mini.ImageView
         */
        backgroundImage: { get: function () {
            return this._bgImage;
        } },
        /**
         * ポップアップ
         * @type Sprite
         */
        popup: { get: function () {
            return this._popup;
        } },
        /**
         * ポップアップ背景
         * @type mini.ImageView
         */
        popupBG: { get: function () {
            return this._popupBG;
        } },
        /**
         * ポップアップ内 スコア数値画像View
         * @type mini.NumericImage
         */
        popupScoreView: { get: function () {
            return this._popupScoreView;
        } },
        /**
         * ポップアップ内 ベストスコア数値画像View
         * @type mini.NumericImage
         */
        popupBestScoreView: { get: function () {
            return this._popupBestScoreView;
        } },
        /**
         * タイトルへボタン
         * @type mini.Button
         */
        btnToTitle: { get: function () {
            return this._btnToTitle;
        } },
        /**
         * リトライボタン
         * @type mini.Button
         */
        btnRetry: { get: function () {
            return this._btnRetry;
        } },
        /**
         * Lineボタン
         * @type mini.Button
         */
        btnLine: { get: function () {
            return this._btnLine;
        } },
        /**
         * Facebookボタン
         * @type mini.Button
         */
        btnFacebook: { get: function () {
            return this._btnFacebook;
        } },
        /**
         * Twitterボタン
         * @type mini.Button
         */
        btnTwitter: { get: function () {
            return this._btnTwitter;
        } }

    });
    mini.ResultScene = ResultScene;


    /********************************************************************************************************************
     * Model
     */

    /**
     * ユーザー情報
     * @constructor
     */
    var User = function () {

        var saved = localStorage.getItem("user");
        if (saved) saved = JSON.parse(saved);
        this._data = saved || this._createInitialData();
    };

    User.prototype = Object.create({}, {

        /**
         * 初期データオブジェクトを生成
         */
        _createInitialData: { value: function () {
            // TODO ユーザーユニークなIDの発行 現状random値
            return { score: 0, bestScore: 0, launchCount: 0, id: Math.random() + "" + Math.random() };
        } },

        /** JSON化 */
        toJSONString: { value: function () {
            return JSON.stringify(this._data);
        } },

        /**
         * データ保存
         */
        _save: { value: function () {
            localStorage.setItem("user", this.toJSONString());
        } },

        /**
         * ユーザー識別子
         */
        id: {
            get: function () {
                return this._data.hasOwnProperty("id") ? this._data.id : null;
            }
        },

        /**
         * ユーザー名
         */
        name: {
            get: function () {
                return this._data.hasOwnProperty("name") ? this._data.name : "no name";
            }
        },
        /**
         * ユーザー名の指定 失敗時はfalseを返す
         * @param {String} value
         */
        setName: { value: function (value) {
            this._data.name = "" + value;
            this._save();
        } },

        /**
         * スコア
         */
        score: {
            get: function () {
                return this._data.hasOwnProperty("score") ? this._data.score : 0;
            },
            set: function (value) {
                value = parseFloat(value);
                this._data.score = value;
                //
                var best = this.bestScore;
                if (!best || this.compareScore(value, best))
                    this._data.bestScore = value; // ベスト更新
                //
                this._save();
            }
        },
        /**
         * スコアのベスト記録
         */
        bestScore: { get: function () {
            return this._data.bestScore || 0;
        } },
        /**
         * スコア比較関数
         * Template Method
         */
        compareScore: { value: function (a, b) {
            return a >= b;
        } },

        /**
         * 起動回数
         * @type Number
         */
        launchCount: { get: function () {
            return this._data.launchCount;
        } },
        _launch: { value: function () {
            this._data.launchCount = (this._data.launchCount || 0) + 1;
            this._save();
        } },

        /**
         * プレイ回数
         * @type Number
         */
        playCount: { get: function () {
            return this._data.hasOwnProperty("playCount") ? this._data.playCount : 0;
        },
            set: function (value) {
                this._data.playCount = value;
                this._save();
            }
        },

        /**
         * ランキング
         * @type Number
         */
        rank: { get: function () {
            return this._data.hasOwnProperty("rank") ? this._data.rank : -1;
        },
            set: function (value) {
                this._data.rank = value;
                this._save();
            }
        }
    });
    mini.User = User;


    /**
     * ゲーム抽象クラス
     * ライフサイクル
     * new -> prepare -> init -> start -> enterFrame [pause -> resume] -> finish -> init -> ...
     *
     * @param {mini.GameScene} scene
     * @constructor
     */
    var Game = function (scene) {
        this._scene = scene;
    };
    Game.prototype = Object.create({}, {

        // property
        /**
         * user
         * @type mini.User
         */
        user: { get: function () {
            return application.user;
        } },
        /**
         * config
         * @type mini.Config
         */
        config: { get: function () {
            return application.config;
        } },
        /**
         * ゲームシーン
         * @type mini.GameScene
         */
        scene: { get: function () {
            return this._scene;
        } },
        /**
         * ステージ
         * @type Stage
         */
        stage: { get: function () {
            return this._scene.stage;
        } },
        /**
         * 表示コンテナ 主にゲーム表示要素はここへ入れる
         * @type Sprite
         */
        container: { get: function () {
            return this._scene.container;
        } },
        /**
         * 背景用表示コンテナ 背景画像等ゲーム表示要素の背面に表示したいものをここへ入れる
         * @type Sprite
         */
        background: { get: function () {
            return this._scene.background;
        } },
        /**
         * 前景用表示コンテナ ナビゲーション等ゲーム表示要素の前面に表示したいものをここへ入れる
         * @type Sprite
         */
        foreground: { get: function () {
            return this._scene.foreground;
        } },

        // method
        /**
         * ゲームを終了し結果画面へ
         * @param {Number} score
         */
        finish: { value: function (score) {
            this.scene.finishGame(score);
        } },

        // template methods
        /**
         * ゲーム準備処理を実装する
         */
        _onPrepare: { value: function () {
            log("game prepare");
        } },
        /**
         * ゲームの初期化処理を実装する
         */
        _onInit: { value: function () {
            log("game init");
        } },
        /**
         * ゲームの開始処理を実装する
         */
        _onStart: { value: function () {
            log("game start");
        } },
        /**
         * ゲームの一時停止処理を実装する
         */
        _onPause: { value: function () {
            log("game pause");
        } },
        /**
         * ゲームの再開処理を実装する
         */
        _onResume: { value: function () {
            log("game resume");
        } },
        /**
         * ゲームの終了処理を実装する
         */
        _onFinish: { value: function () {
            log("game finish");
        } },
        /**
         * 毎フレームの処理を実装する
         * @param {Number} frameCount
         */
        _onEnterFrame: { value: function (frameCount) {
            //any
        } }

    });
    mini.Game = Game;


    /********************************************************************************************************************
     * View
     */

    /**
     * 画像読み込み表示View
     * @param {String} url
     * @param {Bitmap} [bmp]
     * @constructor
     */
    var ImageView = function (url, bmp) {
        this._display = new Sprite();
        this._display.name = "ImageView: " + url || bmp;

        this._bmp = bmp || null;
        this._image = null;

        // clone機能対応
        this._waitingClones = [];

        var self = this;
        if (!this._bmp && url) {
            // url
            var image = new Image(url);
            image.onload = function () {
                self._onImageLoaded(this);
            };
        } else if (this._bmp) {
            // bitmap
            this._bmp.x = -this._bmp.width / 2;
            this._bmp.y = -this._bmp.height / 2;
            this._display.addChild(this._bmp);
            // 非同期感
            setTimeout(function () {
                if (self.onLoad !== null) self.onLoad();
            }, 1);
        } else {
            // empty

        }
    };
    ImageView.prototype = Object.create({}, {

        // property
        display: { get: function () {
            return this._display;
        } },
        bitmap: { get: function () {
            return this._bmp;
        } },
        x: { get: function () {
            return this._display.x;
        }, set: function (value) {
            this._display.x = value;
        } },
        y: { get: function () {
            return this._display.y;
        }, set: function (value) {
            this._display.y = value;
        } },
        width: {
            get: function () {
                return this._bmp.width;
            },
            set: function (value) {
                this._bmp.width = value;
                this._bmp.x = -this._bmp.width / 2;
            }
        },
        height: {
            get: function () {
                return this._bmp.height;
            },
            set: function (value) {
                this._bmp.height = value;
                this._bmp.y = -this._bmp.height / 2;
            }
        },

        // callback
        onLoad: { value: null },

        // internal method
        _onImageLoaded: { value: function (image) {
            this._setBitmap(new Bitmap(new BitmapData(image)));
        } },

        _setBitmap: { value: function (bmp) {
            this._bmp = bmp;
            this._bmp.x = -this._bmp.width / 2;
            this._bmp.y = -this._bmp.height / 2;
            this._display.addChild(this._bmp);

            if (this.onLoad !== null) this.onLoad();

            // clones
            for (var i in this._waitingClones) {
                this._waitingClones[i]._setBitmap(new Bitmap(this._bmp.bitmapData));
                log(this._waitingClones[i].bitmap.bitmapData.width);
            }
            this._waitingClones = [];
        } },

        // method
        /**
         * 画像参照の強制破棄
         */
        dispose: { value: function () {
            // TODO
        } },

        /**
         * 同じ画像をもつImageViewを複製する
         */
        clone: { value: function () {
            //
            if (this._bmp) {
                // 読み込み済み
                return new ImageView(null, new Bitmap(this._bmp.bitmapData));
            } else {
                // 読み込み中
                var clone = new ImageView();
                clone.display.name = this.display.name + ":clone";
                this._waitingClones.push(clone);
                return clone;
            }
        } }
    });
    mini.ImageView = ImageView;


    /**
     * ボタンView
     * 背景画像を内包
     * @param url
     * @param gaLabel 計測用途のボタン識別文字列
     * @param touchBegin
     * @param touchRollOut
     * @constructor
     */
    var Button = function (url, gaLabel, touchBegin, touchRollOut) {
        var self = this;

        this._display = new Sprite();
        this._display.name = "Button: " + (url || "noimage");
        if (url) {
            this._bg = new ImageView(url);
            this._display.addChild(this._bg.display);
            this._bg.onLoad = function () {
                if (self.onLoad !== null) self.onLoad();
            };
        }

        this._touchBegin = touchBegin || function () {
            mini.Tween.removeTweens(self.display);
            this.display.scaleX = self.display.scaleY = 1.08;
        };
        this._touchRollOut = touchRollOut || function () {
            mini.Tween
                .get(self.display, { useTicks: true, override: true })
                .to({ scaleX: 1.0, scaleY: 1.0 }, 30, mini.Ease.elasticOut);
        };
        this._gaLabel = gaLabel || null;

        this._display.addEventListener("touchTap", self._onTap = function (e) {

            if (self.onTap === null) return;

            // audio
            if (Button.buttonAudio) Button.buttonAudio.play();

            self.onTap(e);
        });
        this._display.addEventListener("touchBegin",
            self._onTouchBegin = function (e) {
                if (self.onTouchBegin !== null) self.onTouchBegin(e);
            });
        this._display.addEventListener("touchEnd",
            self._onTouchEnd = function (e) {
                if (self.onTouchEnd !== null) self.onTouchEnd(e);
            });
        this._display.addEventListener("touchRollOver",
            self._onTouchRollOver = function (e) {
                if (self.onTouchRollOver !== null) self.onTouchRollOver(e);
            });
        this._display.addEventListener("touchRollOut",
            self._onTouchRollOut = function (e) {
                if (self.onTouchRollOut !== null) self.onTouchRollOut(e);
            });
    };
    Button.prototype = Object.create({}, {

        // property
        display: { get: function () {
            return this._display;
        } },
        bg: { get: function () {
            return this._bg;
        } },
        x: { get: function () {
            return this._display.x;
        }, set: function (value) {
            this._display.x = value;
        } },
        y: { get: function () {
            return this._display.y;
        }, set: function (value) {
            this._display.y = value;
        } },
        gaLabel: { get: function () {
            return this._gaLabel;
        } },

        // handler
        _onTap: { value: null },
        _onTouchBegin: { value: null },
        _onTouchEnd: { value: null },
        _onTouchRollOver: { value: null },
        _onTouchRollOut: { value: null },

        // callback
        onLoad: { value: null },
        onTap: { value: null },
        onTouchBegin: { value: function () {
            this._touchBegin();
        } },
        onTouchEnd: { value: null },
        onTouchRollOver: { value: null },
        onTouchRollOut: { value: function () {
            this._touchRollOut();
        } },

        // method
        dispose: { value: function () {
            // TODO
        } }

    });
    /**
     * ボタン音
     * @static
     * @type {Audio}
     */
    Button.buttonAudio = null;
    mini.Button = Button;


    /**
     * 数値画像View
     * @param {Number|String} value
     * @param {String} [imageURL]
     * @param {Number} [numberWidth]
     * @constructor
     */
    var NumericImage = function (value, imageURL, numberWidth) {

        this._display = new Sprite();
        this._display.name = "NumericImage";
        this._bd = null;

        this._image = new Image(imageURL || application.config._numbers);
        var self = this;
        this._image.onload = function () {
            self._onImageLoaded();
        };

        this._value = value;

        this._numberWidth = numberWidth || application.config.numberWidth;

        this._bitmaps = [];

        this._hasUnit = null;
    };
    NumericImage.prototype = Object.create({}, {

        // property
        display: { get: function () {
            return this._display;
        } },
        image: { get: function () {
            return this._image;
        } },
        x: { get: function () {
            return this._display.x;
        }, set: function (value) {
            this._display.x = value;
        } },
        y: { get: function () {
            return this._display.y;
        }, set: function (value) {
            this._display.y = value;
        } },
        value: { get: function () {
            return this._value;
        }, set: function (value) {
            this._value = value;
            // display更新
            // 文字列化
            var stringValue = value.toString();
            // filter state
            if (this._image.width <= 0) return;
            // filter value
            if (stringValue.indexOf("e") !== -1) throw new Error("無効な数値が指定されました");
            if (stringValue.indexOf("-") !== -1) throw new Error("無効な数値 マイナス値が指定されました");

            // child準備
            while (this._bitmaps.length < (stringValue.length + 1/*単位分*/)) this._bitmaps.push(new Bitmap(this._bd));

            // remove children
            while (this._display.numChildren > 0) this._display.removeChildAt(0);

            // 単位分
            var unitOffset = this._hasUnit ? 1 : 0;

            // サイズ算出
            var numberWidth = this._numberWidth;
            var dotWidth = this._image.width % numberWidth;
            var imgHeight = this._image.height;
            var width = (stringValue.length + unitOffset) * numberWidth;
            if (stringValue.indexOf(".") !== -1) width += dotWidth - numberWidth;

            var offset = -width / 2;
            for (var i = 0; i < stringValue.length; i++) {
                var char = stringValue.charAt(i);
                var bmp = this._bitmaps[i];
                bmp.y = -imgHeight / 2;
                //
                if (char === ".") {
                    // dot
                    bmp.setClippingRect(new Rectangle((10 + unitOffset) * numberWidth, 0, dotWidth, imgHeight));
                    bmp.x = offset;
                    this._display.addChild(bmp);
                    offset += dotWidth;
                } else {
                    // number
                    bmp.setClippingRect(new Rectangle((unitOffset + parseInt(char)) * numberWidth, 0, numberWidth, imgHeight));
                    bmp.x = offset;
                    this._display.addChild(bmp);
                    offset += numberWidth;
                }
            }

            if (this._hasUnit) {
                var bmp = this._bitmaps[i];
                bmp.y = -imgHeight / 2;
                // number
                bmp.setClippingRect(new Rectangle(0, 0, numberWidth, imgHeight));
                bmp.x = offset;
                this._display.addChild(bmp);
            }

            // TODO 効率化
        } },

        // callback
        onLoad: { value: null },

        // internal method
        _onImageLoaded: { value: function () {
            this._bd = new BitmapData(this._image);
            // has unit?
            this._hasUnit = this._image.width > ( this._numberWidth * 11 );

            this.value = this._value;
            if (this.onLoad !== null) this.onLoad();
        } },

        // method
        dispose: { value: function () {
            // TODO
        } }
    });
    mini.NumericImage = NumericImage;


    /**
     * SpriteSheetアニメーション再生View
     * @param {Image} image
     * @param {Object} jsonObject
     * @constructor
     */
    var SpriteSheetView = function (image, jsonObject) {
        // TODO
    };
    SpriteSheetView.prototype = Object.create({}, {

    });


    /**
     * createjs.TweenJSのユーティリティー
     */
    var Tween = {

        _targetInfoList: [],
        _tweenGroups: {},

        /**
         * トゥイーンを進める
         * @param {Number} delta
         * @param {String} groupName
         */
        tick: function (delta, groupName) {
            // デフォルトのgroupを用意
            groupName = groupName || "default";
            if (!this._tweenGroups[groupName]) return;

            var group = this._tweenGroups[groupName].slice();
            for (var i in group) {
                group[i].tween.tick(delta || 1);
            }
        },

        /**
         *
         */
        get: function () {
            var target = arguments[0];
            var groupName = arguments.length >= 2 ? arguments[1].group : null;
            var tween = createjs.Tween.get.apply(createjs.Tween, arguments);

            // 悩ましいがデフォルトのgroupを用意することに
//            if( !groupName ) return tween;
            if (!groupName) groupName = "default";

            var targetInfo = null;
            for (var i in this._targetInfoList) {
                if (this._targetInfoList[i].target === target) {
                    targetInfo = this._targetInfoList[i];
                    break;
                }
            }

            if (!targetInfo) {
                targetInfo = { target: target, tweens: [] };
                this._targetInfoList.push(targetInfo);
            }

            var group = this._tweenGroups[groupName];
            if (!group) group = this._tweenGroups[groupName] = [];

            var tweenInfo = { target: target, tween: tween, group: group };

            group.push(tweenInfo);
            targetInfo.tweens.push(tweenInfo);

            return tween;
        },

        /**
         *
         */
        removeTweens: function (target) {

            createjs.Tween.removeTweens(target);

            var targetInfo = null;
            for (var i in this._targetInfoList) {
                if (this._targetInfoList[i].target === target) {
                    targetInfo = this._targetInfoList.splice(i, 1)[0];
                    break;
                }
            }
            if (!targetInfo) return;

            for (var i in targetInfo.tweens) {

                var tweenInfo = targetInfo.tweens[i];
                var group = tweenInfo.group;
                group.splice(group.indexOf(tweenInfo), 1);
            }
        }
    };
    mini.Tween = Tween;

    /**
     * createjs.Ease のエイリアス
     * @type {createjs.Ease}
     */
    var Ease = mini.Ease = createjs.Ease;

    /**
     * デバッグ用フッターView
     * 広告と差し替わる想定
     * @constructor
     */
    var DebugFooter = function () {

        this._layer = new Layer(new Stage(640, 100));
        this._layer.verticalAlign = "bottom";
        this._stage = this._layer.content;

        var bg = new Bitmap(new BitmapData(1, 1, true, 0xff000000));
        bg.width = 640;
        bg.height = 100;
        bg.alpha = 0.5;
        this._stage.addChild(bg);

        var self = this;
        // buttons
        this._title = new mini.Button("./assets/mini/dev/btn_title.png");
        this._title.x = 90;
        this._title.y = 50;
        this._title.onTap = function () {
            Scene.goto("/");
        };
        this._stage.addChild(this._title.display);

        this._game = new mini.Button("./assets/mini/dev/btn_game.png");
        this._game.x = 90 * 3;
        this._game.y = 50;
        this._game.onTap = function () {
            Scene.goto("/game");
        };
        this._stage.addChild(this._game.display);

        this._result = new mini.Button("./assets/mini/dev/btn_result.png");
        this._result.x = 90 * 5;
        this._result.y = 50;
        this._result.onTap = function () {
            Scene.goto("/result");
        };
        this._stage.addChild(this._result.display);

        this._reload = new mini.Button("./assets/mini/dev/btn_reload.png");
        this._reload.x = 90 * 6 + 50;
        this._reload.y = 50;
        this._reload.onTap = function () {
            // 表示中のシーンを引き継ぐ
            var url = location.href;
            location.href = url.split("?")[0] + "?rand=" + Math.random() + "&dev_init_path=" + Scene.getCurrent().path;
        };
        this._stage.addChild(this._reload.display);

        // コンテンツの前面へ表示
        addLayer(this._layer);
    };
    DebugFooter.prototype = Object.create({}, {

    });


    var VirtualPad = function (parent) {
        this._topView = new Sprite();
        this._bottomView = new Sprite();
        this._center = new Point();
        this._enabled = false;
        this._angle = 0;

        this._topView.alpha = this._bottomView.alpha = 0.6;
        this._topView.visible = this._bottomView.visible = false;
        parent.addChild(this._bottomView);
        parent.addChild(this._topView);

        var that = this;
        var topImage = new Image("./assets/mini/_pad_top.png");
        topImage.onload = function () {
            var bmp = new Bitmap(new BitmapData(topImage));
            bmp.x = -bmp.width / 2;
            bmp.y = -bmp.height / 2;
            that._topView.addChild(bmp);
        };
        var bottomImage = new Image("./assets/mini/_pad_bottom.png");
        bottomImage.onload = function () {
            var bmp = new Bitmap(new BitmapData(bottomImage));
            bmp.x = -bmp.width / 2;
            bmp.y = -bmp.height / 2;
            that._bottomView.addChild(bmp);
        };
        application.stage.addEventListener("touchBegin", this._onTouchBegin = function (event) {
            that._topView.x = that._bottomView.x = that._center.x = event.stageX;
            that._topView.y = that._bottomView.y = that._center.y = event.stageY;
            that._topView.visible = that._bottomView.visible = that._enabled = true;
        });
        application.stage.addEventListener("touchMove", this._onTouchMove = function (event) {
            that._angle = Math.atan2(event.stageY - that._center.y, event.stageX - that._center.x);
            that._topView.x = that._center.x + 10 * Math.cos(that._angle);
            that._topView.y = that._center.y + 10 * Math.sin(that._angle);
        });
        application.stage.addEventListener("touchEnd", this._onTouchEnd = function (event) {
            that._topView.visible = that._bottomView.visible = that._enabled = false;
        });
    };
    VirtualPad.prototype = Object.create({}, {
        // property
        enabled: { get: function () {
            return this._enabled;
        }},
        angle: { get: function () {
            return this._angle;
        }},

        // handler
        _onTouchBegin: { value: null },
        _onTouchMove: { value: null },
        _onTouchEnd: { value: null }
    });
    mini.VirtualPad = VirtualPad;


    // global
    window.mini = mini;

    // AMD
    return mini;
});