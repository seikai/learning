//Learning1 - scope ///////////////////////////////////////

var globalVar = 'hello, global';

//globalVarとwindow.globalVarは等価
if(globalVar === window.globalVar) {
    console.log("globalVar is window.globalVar");
}

// varの意味 == そのスコープ内のローカル変数を定義
// グローバルの場合はあっても無くても同じ
function scopeSample() {
    
    //scopeSample内にローカル変数の定義
    var localVar = 'hello, localVar';
    
    console.log(globalVar); // -> hello, global
    
    //上書き
    globalVar = 'hello, global, override!';
    
    console.log(globalVar); // -> hello, global, override!
}

//アクセス出来ないのでundefined
console.log(localVar); // -> undefined

//実行後でもundefined
scopeSample();
console.log(localVar); // -> undefined

//scopeSample関数実行時に上書きされた
console.log(globalVar); // -> hello, global, override!


//ブロックスコープは存在しない
if(true) {
    var blockScope = 'not exists';
}

console.log(blockScope); // -> not exists
