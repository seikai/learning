//取得間隔の指定
sensors.motion.interval = 1000/100;

//更新ハンドラの設定
sensors.motion.addEventListener("update", function onUpdateHandler(event) {

    //各センサーの値を取得
    console.log("加速度 - x:" + event.acceleration.x  + " y:" + event.acceleration.y + " z:" + event.acceleration.z);
    console.log("重力加速度 - x:" + event.accelerationIncludingGravity.x  + " y:" + event.accelerationIncludingGravity.y + " z:" + event.accelerationIncludingGravity.z);
    console.log("回転加速度 - x:" + event.rotationRate.x  + " y:" + event.rotationRate.y + " z:" + event.rotationRate.z);
    console.log("重力 - x:" + event.gravity.x  + " y:" + event.gravity.y + " z:" + event.gravity.z);
    console.log("方向 - x:" + event.orientation.x  + " y:" + event.orientation.y + " z:" + event.orientation.z);

    //更新ハンドラの破棄
    sensors.motion.removeEventListener("update", onUpdateHandler);

});
