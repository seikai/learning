var o = {};

// データディスクリプタにより、defineProperty を用いてオブジェクトプロパティを追加する例
Object.defineProperty(o, "a", {value : 37,
                               writable : true,
                               enumerable : true,
                               configurable : true});

console.log("================================");
// o オブジェクトに 'a' プロパティが存在し、その値は 37 です
console.log(o.a); // => 37


// アクセサディスクリプタにより、defineProperty を用いてオブジェクトプロパティを追加する例
var bValue = 1;
Object.defineProperty(o, "b", {get : function(){ return bValue; },
                               set : function(newValue){ bValue = newValue; },
                               enumerable : true,
                               configurable : true});

console.log("================================");
console.log(o.b);    // => 1
console.log(bValue); // => 1


bValue = 2;
console.log("================================");
console.log(o.b);    // => 2
console.log(bValue); // => 2

o.b = 3;
console.log("================================");
console.log(o.b);    // => 3
console.log(bValue); // => 3
