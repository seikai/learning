//Learning1 - this call apply ///////////////////////////////////////

var name = 'global';

function sampleTest() {
    console.log(this.name);
}

// 関数定義内のthisはglobalを参照
sampleTest(); // -> global

// objectに関数を生やしたケース
var sample = {
    name: 'sample',
    test: function() {
        console.log(this.name);
    }
};

sample.test(); // -> sample

// call,applyによるthisの強制
sample.test.call(sample);// -> sample

//オブジェクトから関数のみを取り出したケース
var test = sample.test;
test(); // -> global

// call,applyによるthisの強制
sample.test.call(this);// -> global

// タイマー等イベントハンドラの挙動
setTimeout(function(){
    console.log(this.name); // -> global
}, 1000);

setTimeout(sample.test, 1000); // -> global

// => 呼び出した本人がthisの参照先になる


//new するパターン

//コンストラクタ
var Sample = function(){
    console.log(this.name);
};

Sample.prototype.name = 'Sample';

//newした場合はthisがコンストラクタ自身
new Sample(); // -> Sample





