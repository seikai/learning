//Learning2 - OOP ///////////////////////////////////////

// prototype?
//  -> 原型になるオブジェクト
//  -> JavaやC#でいうclassに近い

//exsample for java
/*
class SampleWorker {

    private String mName;
    
    private List<String> mTasks;
    
    public SampleWorker(String name, List<String> tasks) {
        
        mName = name;
        mTasks = new ArrayList<String>();
        
        if(tasks != null) {
            
            for(String task: tasks) {
                
                addTask(task);
            }
        }
    }
    
    public void addTask(String task) {
        mTasks.add(task);
    }
    
    public String getName() {
        
        return mName;
    }
    
    public void work() {
        
        for(String task: mTasks) {
            
            System.out.println(task);
        }
    }
}
*/

//  -> JavaScriptにclassは無いのでprototypeを擬似的にクラスっぽく使う
//  　 概念としてのクラス
//  　 ※ちなみにわざと間違ったコードにしてます
//

var SampleWorker = function(){
    this.initialize.apply(this, arguments);
};

SampleWorker.prototype = {
    name: '',
    tasks:[],
    initialize: function(name, tasks){
        
        if(tasks) {

            tasks.forEach(function(task){
                
                this.name = name;
                this.addTask(task);
                
            }, this);
        }

    },
    addTask: function(task){
        
        this.tasks.push(task);
    },
    getName: function() {
        
        return this.name;
    },
    work: function(){

        console.log("================================");

        this.tasks.forEach(function(task){
            console.log(task);
        });
    }
};

//解説
// new => this === SampleWorker
//    SampleWorkerのthis => SampleWorker.prototypeが参照出来る
//    this.initialize = SampleWorker.prototype.initialize
//
//    this.initializeを呼ぶ理由 => newの強制  ==  SampleWorkerを関数呼び出しするとエラーを吐く
//    継承のした場合の機能分離
//    initialize は prototype に属する関数なので、メモリ効率が良い
//
//    その他足りないものはルールで補う（アクセス修飾子等）
//    例：private 命名規則として _varNameなどのように_を付ける
//    
//    ES5は実現出来る範囲が広がる defineProperty等
//

var worker1 = new SampleWorker('A', ['プログラムを書く']);
worker1.getName(); //  => 'A'
worker1.work(); // => 'プログラムを書く'


//上の定義の駄目なとこ
var worker2 = new SampleWorker('B', ['仕様書を書く']);
worker2.getName(); //  => 'B'
worker2.work(); // => 'プログラムを書く'、'仕様書を書く'

worker1.work(); // => 'プログラムを書く'、'仕様書を書く'

//prototypeのオブジェクトは共有される
//FIX

/*
SampleWorker.prototype.initialize = function(name, tasks){
    
    //プリミティブな値以外は原則イニシャライザの中で初期化
    this.tasks = [];
    
    if(tasks) {
        
        tasks.forEach(function(task){
            
            this.name = name;
            this.addTask(task);
            
        }, this);
    }

};
*/
//継承


//SampleWorkerを継承したSuperWorker

//case for Java
/*
class SuperWorker extends SampleWorker {
    // any
}
*/

//JavaScript
var SuperWorker = function () {
    this.initialize.apply(this, arguments);
};


//SampleWorkerをprototype継承
//この時点ではSuperWorker.prototype == SampleWorker.prototype
SuperWorker.prototype = new SampleWorker();


//イニシャライザのオーバーライド
//SuperWorkerは言われなくても仕事を見つける
//SuperWorkerは名前もsuper
SuperWorker.prototype.initialize = function (name) {
    SampleWorker.prototype.initialize.call(name + '@super', ['プログラムを書く', '仕様書を書く']);
};


//workメソッドのオーバーライド
//SuperWorkerは二倍働く
SuperWorker.prototype.work = function(){

    console.log("================================");

    this.tasks.forEach(function(task){
        console.log(task);
        console.log(task);
    });
};


var superWorker = new SuperWorker('C');
superWorker.getName();  //  => 'C@super'
superWorker.work();     // => 'プログラムを書く'、'プログラムを書く'、'仕様書を書く'、'仕様書を書く'



