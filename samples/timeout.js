//Timerの挙動

var start = Date.now();

//100ms後に実行をセット
setTimeout(function(){
    console.log('100ms後？');
    console.log(Date.now() - start);
}, 100);

//例えば時間のかかる処理
var count = 0;
while(count < 100000000) {
    //any
    count++;
}

console.log('時間のかかる処理：' + String(Date.now() - start));

// setTimeoutで設定した時間ではなく上記の処理にかかった時間＋αが出力される
